$(document).ready(function() {
		console.log("inside main function");

		$(".right.floated.ui.tiny.image.qrcode").qrcode({
			"text": 'Some Information regarding Repos',
			"size": 65
		});

		//Initializing Selectize

		var $select = $("#select-user").selectize({
			valueField: 'username',
			labelField: 'name',
			searchField: 'name',
			options: [],
			create: false,
			render: {
				option: function(item, escape) {
					console.log("inside option");
					console.log(item.username + ' ' + item.repos + ' ' + item.public_repo_count);
					var private = item.repos - item.public_repo_count;
					var slicedId = item.id.slice(5,item.id.length);
					console.log("private = "+private+" id = "+slicedId );
					return '<div class="ui middle aligned list">' +
						'<div class="item">' + 
							'<img class="ui avatar image" src="' + 'https://avatars.githubusercontent.com/u/' + slicedId + '?v=3' +'">' +
							'&nbsp &nbsp &nbsp' +
							'<div class="content">' +
								'<div class="description">' + 
									'<a class="header">' + escape(item.username) + '</a>' +
								'</div>' +
								'<div class="below">' +
									'Public: <span class="public-no">' + escape(item.public_repo_count) + '</span>' +
									'&nbsp &nbsp &nbsp' +
									'Private: <span class="private-no">' + private + '</span>' +
								'</div>' +
							'</div>' +
						'</div>' + 
					'</div>';
					}
				},
				score: function(search) {
					var score = this.getScoreFunction(search);
					return function(item) {
						console.log(item);
						return  (1 + Math.min(item.score,1));
					};
				},
				load: function(query, callback) {
					console.log("in load method");

					var selectize = $select[0].selectize;
  					selectize.clearOptions();

					var dataToSend = [];
					var slicedArray = [];
					if (!query.length) return callback();
					$.ajax({
						url: 'https://api.github.com/legacy/user/search/' + encodeURIComponent(query),
						type: 'GET',
						error: function() {
							console.log("1st ajax ERROR");
							callback();
						},
						success: function(data) {
							console.log("inside 1st ajax Success");
							console.log(data.users.slice(0,Math.min(10,data.users.length)));
							callback(data.users.slice(0,Math.min(10,data.users.length)));
						} 
					});
				},
				onItemAdd: function(value, $item) {
					console.log("hello");
					console.log(value);
					$.ajax({
						url: 'https://api.github.com/legacy/user/search/' + encodeURIComponent(value),
						type: 'GET',
						error: function() {
							console.log("onItemAdd - Error in ajax");
						},
						success: function(item) {
							console.log("onItemAdd - inside success ajax");
							console.log(item + ' ' + item.users + ' ' + item.users.id);
							var private = item.users.repos - item.users.public_repo_count || 0;
							var slicedId = item.users[0].id.slice(5,item.users[0].id.length);
							
							console.log("onItemAdd - private = "+private+" id = "+slicedId );

							$('.ui.twelve.wide.column.cards').html(
								'<div class="card">' + 
									'<div class="content">' + 
										'<img class="right floated small ui rounded image" src="' + 'https://avatars.githubusercontent.com/u/' + slicedId + '?v=3' + '">' + 
										'<div class="header">' + item.users[0].username + '</div>' +
										'<div class="meta">' + item.users[0].fullname + '</div>' +
										'<br>' +
										'<div class="descriptions">' +
											'Hails from ' + item.users[0].location + 
											'<br> Has ' + item.users[0].followers + ' Followers ' +
											'<br> and codes in ' +  item.users[0].language +
										'</div>' +
									'</div>' +
									'<div class="extra content">' +
										'<div class="right floated ui tiny image qrcode"></div>' +
										'Scan this QR code to know more' +
									'</div>' +
								'</div>' +
								'<button class="ui red fluid button">Save To Jpeg</button>'
							);

							$('.ui.twelve.wide.column.cards .card').find(".right.floated.ui.tiny.image.qrcode").qrcode({
								"text": 'Public Repos= ' + item.users[0].public_repo_count + '  ' + 'Private Repos= ' + private,
								"size": 65
							});
						}
					});
					$('#warning').fadeIn(200);
				}
		});

		$(".ui.twelve.wide.column.cards").livequery(' .ui.red.fluid.button', function() {
			console.log("inside livequery event");
			$(this).on('click', function() {
				console.log("inside on click event");
				html2canvas($(".ui.twelve.wide.column.cards .card"), {
					onrendered: function(canvas) {
						// theCanvas = canvas;
						document.body.appendChild(canvas);

						Canvas2Image.saveAsJPEG(canvas);
						document.body.removeChild(canvas);
					}
				});
			});
		});
		
});		

