Have completed the Dipper Task partII.   
1. Created a user search field for Github using its legacy API(i.e. v2).    
2. Added functionality for creation of a business card like object.    
3. Added a QR code to the card for data retrieval regardin Repositories Information.   
4. Added functionality to download the Card as a .jpeg image.  
  
  
Used Technologies -  
1. livequery by Brandon Aaron - https://github.com/brandonaaron/livequery   
2. jquery.qrcode - https://larsjung.de/jquery-qrcode/  \
3. Selectize.js by Brian Reavis - http://selectize.github.io/selectize.js/     
4. canvas2image by hogru - https://github.com/hongru/canvas2image   
5. html2canvas by Niklas von Hertzen - http://html2canvas.hertzen.com/     
6. semantic-ui - http://semantic-ui.com/  
7. jquery   
  
**The Site is Hosted Here -**  http://dipper-task2.bitballoon.com